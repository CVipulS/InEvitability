InEvitability organizes toDo items.
    
Different toDos have different periodicity.  
Constant switching gets nothing done.
InEvitability keeps track of when I may reconsider next  
or if I must switch now.

The only dependency is FLTK.  
    g++ -I/usr/local/include -L/usr/local/lib -lfltk InEvitability.cpp

Just make an issue on gitlab for queries,  
and the wiki will be updated according to significance.

The project is maintained by Chawathe, Vipul S.  
Anyone may make pull request against issues when the solution is as per the  
discussion following the issue.

[Contribution guidelines for this project](CONTRIBUTING.md)